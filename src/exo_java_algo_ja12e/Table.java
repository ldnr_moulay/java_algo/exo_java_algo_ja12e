/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exo_java_algo_ja12e;

import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author stag
 */
public class Table {

    //proprietes
    Scanner scanner = new Scanner(System.in);
    int nombre;
    int i = 1;

    //constructor
    public Table() {
 
    }

    //Methods
    public void tabler() {
        scanner.useLocale(Locale.ENGLISH);

        System.out.print(
                "Saisissez le nombre dont vous voulez la table : ");
        nombre = scanner.nextInt();

        System.out.println(
                "La table de multiplication de " + nombre);

        do {
            System.out.println(nombre + "x" + i + " = " + (nombre * i));
            i++;
        } while (i
                <= 10);

    }

}
